CHANGELOG
=========

This project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[0.13.1] - 2021-11-01
  - added docs to add new memory map ip to the SoC.

[0.13.0] - 2021-10-12
  - bumped devices to v2.0.0

[0.12.4] - 2021-09-13
  - added Flashing capacity via OpenOCD for ARTY-100T boards.
  - added booting from flash when DDR mode is selected.
  - minor updates to the documentation
  - changed gateway to new module
  - using new linux compatible plic

[0.12.3] - 2021-09-01
  - added missing verilog files to the build verilog directory.

[0.12.2] - 2021-09-01
  - Connecting Ethernet Interrupt to Gateway.

[0.12.1] - 2021-08-31
  - bumped devices to v1.5.1 (Includes PWM with synchronizers, SPI with Controller and Peripheral modes)
  - Fixed Ethernet phy_mdio_i pin in FPGA
  - Doc Updates for SPI and PWM

[0.12.0] - 2021-08-27
  - adding ethernet lite support
  - updated instances of ram modules based on new memcnofig modules from bsvwrappers

[0.11.5] - 2021-08-27
  - bumped devices

[0.11.4] - 2021-07-29
  - Fix Interrupt Gateway for SPI and PWM interrupts.
  - Fix for SPI interrupt duplication

[0.11.3] - 2021-06-15
  - Upgraded devices to v1.4.6
  - Fix Makefile for 'link_verilator' option.

[0.11.2] - 2021-06-10
  - Upgraded devices to v1.4.5
  - Addition of Gateway between Interrupt sources and plic
  - Changes of spi_cluster interrupts

[0.11.1] - 2021-05-27
  - Upgraded devices to v1.4.4
  - typo fix and update to documentation

[0.11.0] - 2021-03-27
  - Removed ClockDiv.bsv from the common_bsv
  - Upgraded devices to v1.4.2

[0.10.0] - 2021-03-03
  - adding qspi controller to the SoC
  - adding qspi pins to the JA Pmod header of arty.
  - increased plic sources to 32 to accommodate interrupts from qspi
  - added bitfield based register diagrams
  - typo fixes in docs

[0.9.9] - 2021-02-08
  - updated block diagram of soc with spi cluster
  - fixed sphinx build issues with bibtex contrib library

[0.9.8] - 2021-02-06
  - lock bibtex contrib to 1.0.0 in docs/requirements.txt

[0.9.7] - 2020-09-25
  - Interface SPI to SoC

[0.9.6] - 2020-11-11
  - fixed stack space in crt to 12KB to fit within OCM of 16KiB

[0.9.5] - 2020-09-16
  - Interface PWM to SoC
  - Paramterized GPIO Interrupts
  - reduced gpio interrupts to 16
  - updated docs for pwms and gpios

[0.9.4] 2020-09-12
  - the xilinx tap module is not compatible with the latest riscv-copenocd

[0.9.3] - 2020-07-06
  - increased gpios to 22
  - remapped interrupt connections of gpios and uart.
  - updated docs to reflect more information about io signals and interrupt connections
  - fixed makefile targets for proper boot file generation.
  - fixed connect_gdb target and software/Makefile to point to the arty100t board alias

[0.9.2] - 2020-07-01
  - clarification of <path> and <jobs> in setup guide.
  - added resource summary for arty-100t board.
  - fixed typos in overview and memory map.
  - xilinx implementation is now perf-optimized.
  - generate_hexfile target to create bin folder if not already available.
  - version for each device updated in docs
  - added ci to check bsv build for merge requests

[0.9.1] - 2020-06-29
  - minor doc updates.


[0.9.0] - 2020-06-29
  - initial release of the Chromite M SoC with: clint, uart, gpio, ddr, plic, ocm and boot rom.
