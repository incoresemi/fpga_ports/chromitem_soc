// Copyright (c) 2020 InCore Semiconductors Pvt. Ltd. see LICENSE.incore for more details on licensing terms
#include "boot.h"
void main()
{
  //Set the output_en of the various IOs as per the FPGA configuration.
  //The GPIOs 0-15 should be configured as inputs.
  volatile short* temp1_gpio_out_en= (short*) CHROMITEM_GPIO_OUTPUT_EN;
  *temp1_gpio_out_en= 0x0000;

  //The GPIOs 16-19 should be configured as outputs
  volatile char* temp2_gpio_out_en= (char*) (CHROMITEM_GPIO_OUTPUT_EN + 2);
  *temp2_gpio_out_en= 0xF;

  printf("%s",bootlogo);
  printf("%s",soc_name);
  printf("%s",VERSIONVAL);
  printf("%s",BUILDVAL);
  printf("%s",copyright);
  printf("%s",license);
  printf("\n");
  if(boot_mode()==0) {
    printf("\n >>> Entering Debug Mode.\n\n");
  	asm volatile("ebreak");
  }
  else if (boot_mode() == 1) {
    printf("\n >>> Booting from on-chip memory at 0x%x.\n\n",CHROMITEM_OCM_BASE);
    jump_to(ocm_base);
  }
  else if (boot_mode() == 2) {
    printf("\n >>> Booting from DDR memory at 0x%x.\n\n",CHROMITEM_DDR_BASE);
    jump_to(ddr_base);
  }
}

void jump_to(Addr* jump_addr)
{
    volatile register unsigned long* a0 asm("a0") = (unsigned long*) jump_addr;
    asm volatile("jr a0");
}
