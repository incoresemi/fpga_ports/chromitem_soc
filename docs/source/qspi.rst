.. _qspi:

##############################################
QUAD Serial Peripheral Interface (QSPI) Module
##############################################

.. include:: <isonum.txt>

This chapter will discuss the operation of the QUAD Serial Peripheral Interface (QSPI) module instantiated in this design.

IP Details and Available Configuration
======================================


:numref:`QSPI_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _QSPI_ip_details:

.. table:: QSPI IP details

  ========================================  ==========================
  ..                                        **Value**
  ========================================  ==========================
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.4.0
  Ip Type                                   memory_mapped
  Numer of Config Registers                 13
  Direct Memory Region                      Read-only region available
  Configuration Register Alignment (bytes)  4
  ========================================  ==========================

:numref:`QSPI_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _QSPI_configuration_details:

.. table:: QSPI IP Configuration Options

  ===============  ===============  ======================================================================================================================================================================================
  Configuration    Options          Description
  ===============  ===============  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L       Choice of bus interface protocol supported on this IP
  Base address     Integer          The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X34  The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  ===============  ===============  ======================================================================================================================================================================================



QSPI Instance Details
=====================



:numref:`QSPI_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _QSPI_instance_details:

.. table:: QSPI Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0X40000
  Bound Address         0X400FF
  Bytes reserved        0XFF
  Bus Interface         APB
  ====================  ====================


QSPI Features
=============


Quad-SPI, also known as QSPI, is a peripheral that can be found in most modern
microcontrollers. It has been specifically designed to communicate with flash memories. It is
predominantly used in applications that involves a lot of memory-intensive data like multimedia
and for which on-chip memory is not enough. It can be also used to store code externally and it
has the ability to make the external memory behave as fast as the internal memory through
special mechanisms.

The Quad-SPI is a serial interface that allows the communication on four data lines between
the host and an external Quad-SPI memory. The QSPI supports the traditional SPI (serial
peripheral interface) as well as the dual-SPI mode which allows to communicate on two lines.
QSPI uses up to six lines in quad mode.
QSPI is faster than traditional SPI as Quad-SPI uses 4 data lines (I0, I1, I2, and I3) in
contradiction to just 2 data lines (MOSI and MISO) on the traditional SPI.

Features of QSPI are as follows:

1. There are three functional modes: indirect, status-polling, and memory-mapped.
2. Dual-flash mode, where 8 bits can be sent/received simultaneously by accessing two Flash memories in parallel.
3. SDR and DDR support.
4. Fully programmable opcode for both indirect and memory mapped mode.
5. Fully programmable frame format for both indirect and memory mapped mode.
6. Integrated FIFO for reception and transmission.
7. 8, 16, and 32-bit data accesses are allowed.
8. DMA channel for indirect mode operations.
9. Interrupt generation on FIFO threshold, timeout, operation complete, and access error.
  1. Indirect mode: all the operations are performed using the QUADSPI registers
  2. Status polling mode: the external Flash memory status register is periodically read and an interrupt can be generated in case of flag setting
  3. Memory-mapped mode: the external Flash memory is mapped to the microcontroller
     address space and is seen by the system as if it was an internal memory. Both throughput and
     capacity can be increased two-fold using dual-flash mode, where two QSPI Flash memories
     are accessed simultaneously.

Both throughput and capacity can be increased two-fold using dual-flash mode, where two Quad-
SPI Flash memories are accessed simultaneously.

Register Map
============



The register map for the QSPI control registers is shown in
:numref:`QSPI_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _QSPI_register_map:

.. table:: QSPI Register Mapping for all configuration registes

  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | Register-Name   | Offset(hex)   |   Size(Bits) | Reset(hex)   | Description                               |
  +=================+===============+==============+==============+===========================================+
  | QUADSPI_CR      | 0X0           |           32 | 0X0          | control register for the host controller  |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_DCR     | 0X4           |           32 | 0X0          | control register for device configuration |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_SR      | 0X8           |           32 | 0X0          | status register                           |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_FCR     | 0XC           |           32 | 0X0          | flag clear register                       |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_DLR     | 0X10          |           32 | 0X0          | Data length register register             |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_CCR     | 0X14          |           32 | 0X0          | Communication Configuration Register      |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_AR      | 0X18          |           32 | 0X0          | Address register                          |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_ABR     | 0X1C          |           32 | 0X0          | Alternate Byte register                   |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_DR      | 0X20          |           32 | 0X0          | Data register                             |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_PSMKR   | 0X24          |           32 | 0X0          | Polling status mask register              |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_PSMAR   | 0X28          |           32 | 0X0          | Polling status match register             |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_PIR     | 0X2C          |           32 | 0X0          | Polling Interval register                 |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+
  | QUADSPI_LPTR    | 0X2C          |           32 | 0X0          | Low power timeout register                |
  +-----------------+---------------+--------------+--------------+-------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface





The register access attributes for the QSPI control registers are shown in 
:numref:`QSPI_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _QSPI_register_access_attr:

.. table:: QSPI Register Access Attributes for all configuration Registers

  ===============  =============  ============  ============  ============
  Register-Name    Access Type    Reset Type    Min Access    Max Access
  ===============  =============  ============  ============  ============
  QUADSPI_CR       read-write     synchronous   1B            4B
  QUADSPI_DCR      read-write     synchronous   1B            4B
  QUADSPI_SR       read-only      synchronous   1B            4B
  QUADSPI_FCR      write-only     synchronous   1B            4B
  QUADSPI_DLR      read-write     synchronous   1B            4B
  QUADSPI_CCR      read-write     synchronous   1B            4B
  QUADSPI_AR       read-write     synchronous   1B            4B
  QUADSPI_ABR      read-write     synchronous   1B            4B
  QUADSPI_DR       read-write     synchronous   1B            4B
  QUADSPI_PSMKR    read-write     synchronous   1B            4B
  QUADSPI_PSMAR    read-write     synchronous   1B            4B
  QUADSPI_PIR      read-write     synchronous   1B            4B
  QUADSPI_LPTR     read-write     synchronous   1B            4B
  ===============  =============  ============  ============  ============




QUADSPI_CR Register
===================

This is a 32-bit register holds various subfields which control the nature of transaction to be performed with the device

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "ENABLE", "attr":"read-write" },
    {"bits": 1, "name": "ABORT", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "TCEN", "attr":"read-write" },
    {"bits": 4, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 4, "name": "FTHRES", "attr":"read-write" },
    {"bits": 4, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "TEIE", "attr":"read-write" },
    {"bits": 1, "name": "TCIE", "attr":"read-write" },
    {"bits": 1, "name": "FTIE", "attr":"read-write" },
    {"bits": 1, "name": "SMIE", "attr":"read-write" },
    {"bits": 1, "name": "TOIE", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "APMS", "attr":"read-write" },
    {"bits": 1, "name": "PMM", "attr":"read-write" },
    {"bits": 8, "name": "PRESCALAR", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_CR_subfields:

.. table:: QUADSPI_CR subfeild description

  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                           |
  +=========+==============+=============+=======================================================================+
  | [0:0]   | enable       | read-write  | Enable the QUADSPI.                                                   |
  |         |              |             |                                                                       |
  |         |              |             |   - 0: QUADSPI is disabled                                            |
  |         |              |             |   - 1: QUADSPI is enabled                                             |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [1:1]   | abort        | read-write  | Abort request                                                         |
  |         |              |             |                                                                       |
  |         |              |             | This bit aborts the on-going command sequence. It is                  |
  |         |              |             | automatically reset once the abort is complete. This bit stops        |
  |         |              |             | the current transfer. In polling mode or memory-mapped mode,          |
  |         |              |             | this bit also reset the APM bit or the DM bit.                        |
  |         |              |             |                                                                       |
  |         |              |             | - 0: No abort requested                                               |
  |         |              |             | - 1: Abort requested                                                  |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [2:2]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [3:3]   | tcen         | read-write  | Time out counter enable                                               |
  |         |              |             |                                                                       |
  |         |              |             | This bit is valid only when memory-mapped mode (FMODE = 11) is        |
  |         |              |             | selected.                                                             |
  |         |              |             | Activating this bit causes the chip select (nCS) to be released       |
  |         |              |             | (and thus reduces consumption) if there has not been an access        |
  |         |              |             | after a certain amount of time, where this time is defined by         |
  |         |              |             | TIMEOUT[15:0] (QUADSPI_LPTR).                                         |
  |         |              |             |                                                                       |
  |         |              |             | Enable the timeout counter.                                           |
  |         |              |             |                                                                       |
  |         |              |             | By default, the QUADSPI never stops its prefetch operation,           |
  |         |              |             | keeping the previous read operation active with nCS maintained        |
  |         |              |             | low, even if no access to the Flash memory occurs for a long          |
  |         |              |             | time. Since Flash memories tend to consume more when nCS is           |
  |         |              |             | held low, the application might want to activate the timeout          |
  |         |              |             | counter (TCEN = 1, bit 3 of QUADSPI_CR) so that nCS is released       |
  |         |              |             | after a period of TIMEOUT[15:0]. (QUADSPI_LPTR) cycles have           |
  |         |              |             | elapsed without an access since when the FIFO becomes full            |
  |         |              |             | with prefetch data.                                                   |
  |         |              |             |                                                                       |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Timeout counter is disabled, and thus the chip select            |
  |         |              |             |   (nCS) remains active indefinitely after an access in                |
  |         |              |             |   memory-mapped mode.                                                 |
  |         |              |             | - 1: Timeout counter is enabled, and thus the chip select is          |
  |         |              |             |   released in memory-mapped mode after TIMEOUT[15:0] cycles of        |
  |         |              |             |   Flash memory inactivity. This bit can be modified only when         |
  |         |              |             |   BUSY = 0.                                                           |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [7:4]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [11:8]  | fthres       | read-write  | FIFO threshold level                                                  |
  |         |              |             |                                                                       |
  |         |              |             | Defines, in indirect mode, the threshold number of bytes in the       |
  |         |              |             | FIFO that will cause the FIFO threshold flag                          |
  |         |              |             | (FTF, QUADSPI_SR[2]) to be set.                                       |
  |         |              |             |                                                                       |
  |         |              |             | In indirect write mode (FMODE = 00) -                                 |
  |         |              |             |                                                                       |
  |         |              |             | - 0: FTF is set if there are 1 or more free bytes available to        |
  |         |              |             |   be written to in the FIFO                                           |
  |         |              |             | - 1: FTF is set if there are 2 or more free bytes available to        |
  |         |              |             |   be written to in the FIFO                                           |
  |         |              |             | - ...                                                                 |
  |         |              |             | - 15: FTF is set if there are 16 free bytes available to be           |
  |         |              |             |   written to in the FIFO                                              |
  |         |              |             |                                                                       |
  |         |              |             | In indirect read mode (FMODE = 01):                                   |
  |         |              |             |                                                                       |
  |         |              |             | - 0: FTF is set if there are 1 or more valid bytes that can be        |
  |         |              |             |   read from the FIFO                                                  |
  |         |              |             | - 1: FTF is set if there are 2 or more valid bytes that can be        |
  |         |              |             |   read from the FIFO                                                  |
  |         |              |             | - ...                                                                 |
  |         |              |             | - 15: FTF is set if there are 16 valid bytes that can be read         |
  |         |              |             |   from the FIFO                                                       |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [15:12] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [16:16] | teie         | read-write  | Transfer error interrupt enable                                       |
  |         |              |             |                                                                       |
  |         |              |             | This bit enables the transfer error interrupt.                        |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Interrupt disable                                                |
  |         |              |             | - 1: Interrupt enable                                                 |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [17:17] | tcie         | read-write  | Transfer complete interrupt enable                                    |
  |         |              |             |                                                                       |
  |         |              |             | This bit enables the transfer complete interrupt.                     |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Interrupt disable                                                |
  |         |              |             | - 1: Interrupt enable                                                 |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [18:18] | ftie         | read-write  | FIFO Threshold interrupt enable                                       |
  |         |              |             |                                                                       |
  |         |              |             | This bit enables the FIFO threshold interrupt.                        |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Interrupt disable                                                |
  |         |              |             | - 1: Interrupt enable                                                 |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [19:19] | smie         | read-write  | Status match interrupt enable                                         |
  |         |              |             |                                                                       |
  |         |              |             | This bit enables the status match interrupt.                          |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Interrupt disable                                                |
  |         |              |             | - 1: Interrupt enable                                                 |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [20:20] | toie         | read-write  | TimeOut interrupt enable                                              |
  |         |              |             |                                                                       |
  |         |              |             | This bit enables the TimeOut interrupt.                               |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Interrupt disable                                                |
  |         |              |             | - 1: Interrupt enable                                                 |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [21:21] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [22:22] | apms         | read-write  | Automatic poll mode stop                                              |
  |         |              |             |                                                                       |
  |         |              |             | This bit determines if automatic polling is stopped after a match.    |
  |         |              |             |                                                                       |
  |         |              |             | - 0: Automatic polling mode is stopped only by abort or by            |
  |         |              |             |   disabling the QUADSPI.                                              |
  |         |              |             | - 1: Automatic polling mode stops as soon as there is a match.        |
  |         |              |             |   This bit can be modified only when BUSY = 0.                        |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [23:23] | pmm          | read-write  | Polling match mode                                                    |
  |         |              |             |                                                                       |
  |         |              |             | This bit indicates which method should be used for determining        |
  |         |              |             | a “match” during automatic polling mode.                              |
  |         |              |             |                                                                       |
  |         |              |             | - 0: AND match mode. SMF is set if all the unmasked bits              |
  |         |              |             |   received from the Flash memory match the corresponding bits         |
  |         |              |             |   in the match register.                                              |
  |         |              |             | - 1: OR match mode. SMF is set if any one of the unmasked bits        |
  |         |              |             |   received from the Flash memory matches its corresponding bit        |
  |         |              |             |   in the match register. This bit can be modified only when BUSY = 0. |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [31:24] | prescalar    | read-write  | This field defines the scaler factor for generating CLK based         |
  |         |              |             | on the interconnect clock (value+1).                                  |
  |         |              |             |                                                                       |
  |         |              |             | - 1: FCLK = FAHB, AHB clock used directly as QUADSPI CLK              |
  |         |              |             |   (prescaler bypassed)                                                |
  |         |              |             | - 2: FCLK = FAHB/2                                                    |
  |         |              |             | - 3: FCLK = FAHB/3                                                    |
  |         |              |             | -  ....                                                               |
  |         |              |             | - 255: FCLK = FAHB/256                                                |
  |         |              |             |                                                                       |
  |         |              |             | For odd clock division factors, CLK’s duty cycle is not 50%.          |
  |         |              |             | The clock signal remains low one cycle longer than it stays           |
  |         |              |             | high. This field can be modified only when BUSY = 0.                  |
  +---------+--------------+-------------+-----------------------------------------------------------------------+





QUADSPI_DCR Register
====================

control register for device configuration

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CK_MODE", "attr":"read-write" },
    {"bits": 7, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 3, "name": "CSHT", "attr":"read-write" },
    {"bits": 5, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 5, "name": "FSIZE", "attr":"read-write" },
    {"bits": 11, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_DCR_subfields:

.. table:: QUADSPI_DCR subfeild description

  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                                      |
  +=========+==============+=============+==================================================================================================+
  | [0:0]   | ck_mode      | read-write  | Mode0 / mode3                                                                                    |
  |         |              |             |                                                                                                  |
  |         |              |             | This bit indicates the level that CLK takes between commands (when nCS = 1).                     |
  |         |              |             |                                                                                                  |
  |         |              |             | - 0: CLK must stay low while nCS is high (chip select released). This is referred to as mode 0.  |
  |         |              |             | - 1: CLK must stay high while nCS is high (chip select released). This is referred to as mode 3. |
  |         |              |             |                                                                                                  |
  |         |              |             | This field can be modified only when BUSY = 0.                                                   |
  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+
  | [7:1]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                           |
  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+
  | [10:8]  | csht         | read-write  | Chip select high time                                                                            |
  |         |              |             |                                                                                                  |
  |         |              |             | CSHT+1 defines the minimum number of CLK cycles which the chip select (nCS) must                 |
  |         |              |             | remain high between commands issued to the Flash memory.                                         |
  |         |              |             |                                                                                                  |
  |         |              |             | - 0: nCS stays high for at least 1 cycle between Flash memory commands                           |
  |         |              |             | - 1: nCS stays high for at least 2 cycles between Flash memory commands                          |
  |         |              |             | - ...                                                                                            |
  |         |              |             | - 7: nCS stays high for at least 8 cycles between Flash memory commands.                         |
  |         |              |             |                                                                                                  |
  |         |              |             | This field can be modified only when BUSY = 0.                                                   |
  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+
  | [15:11] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                           |
  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+
  | [20:16] | fsize        | read-write  | Flash memory size                                                                                |
  |         |              |             |                                                                                                  |
  |         |              |             | This field defines the size of external memory using the following formula:                      |
  |         |              |             |                                                                                                  |
  |         |              |             | ``Number of bytes in Flash memory = 2[FSIZE+1]``                                                 |
  |         |              |             |                                                                                                  |
  |         |              |             | FSIZE+1 is effectively the number of address bits required to address the Flash memory.          |
  |         |              |             | The Flash memory capacity can be up to 4GB (addressed using 32 bits) in indirect mode,           |
  |         |              |             | but the addressable space in memory-mapped mode is limited to 256MB.                             |
  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+
  | [31:21] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                           |
  +---------+--------------+-------------+--------------------------------------------------------------------------------------------------+





QUADSPI_SR Register
===================

status register

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "TEF", "attr":"read-only" },
    {"bits": 1, "name": "TCF", "attr":"read-only" },
    {"bits": 1, "name": "FTF", "attr":"read-only" },
    {"bits": 1, "name": "SMF", "attr":"read-only" },
    {"bits": 1, "name": "TOF", "attr":"read-only" },
    {"bits": 1, "name": "BUSY", "attr":"read-only" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 3, "name": "FIFO_LEVEL", "attr":"read-only" },
    {"bits": 21, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_SR_subfields:

.. table:: QUADSPI_SR subfeild description

  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                                                                                                                                                                                                                                                                                                                                 |
  +=========+==============+=============+=============================================================================================================================================================================================================================================================================================================================================================================================+
  | [0:0]   | tef          | read-only   | Transfer error flag                                                                                                                                                                                                                                                                                                                                                                         |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | This bit is set in indirect mode when an invalid address is being accessed in indirect mode. It is cleared by writing 1 to CTEF.                                                                                                                                                                                                                                                            |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [1:1]   | tcf          | read-only   | Transfer complete flag                                                                                                                                                                                                                                                                                                                                                                      |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | This bit is set in indirect mode when the programmed number of data has been transferred or in any mode when the transfer has been aborted.It is cleared by writing 1 to CTCF.                                                                                                                                                                                                              |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [2:2]   | ftf          | read-only   | FIFO threshold flag                                                                                                                                                                                                                                                                                                                                                                         |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | In indirect mode, this bit is set when the FIFO threshold has been reached, or if there is any data left in the FIFO after reads from the Flash memory are complete. It is cleared automatically as soon as threshold condition is no longer true. In automatic polling mode this bit is set every time the status register is read, and the bit is cleared when the data register is read. |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [3:3]   | smf          | read-only   | Status match flag                                                                                                                                                                                                                                                                                                                                                                           |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | This bit is set in automatic polling mode when the unmasked received data matches the corresponding bits in the match register (QUADSPI_PSMAR). It is cleared by writing 1 to CSMF.                                                                                                                                                                                                         |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [4:4]   | tof          | read-only   | Timeout flag                                                                                                                                                                                                                                                                                                                                                                                |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | This bit is set when timeout occurs. It is cleared by writing 1 to CTOF.                                                                                                                                                                                                                                                                                                                    |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [5:5]   | busy         | read-only   | Busy                                                                                                                                                                                                                                                                                                                                                                                        |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | This bit is set when an operation is on going. This bit clears automatically when the operation with the Flash memory is finished and the FIFO is empty.                                                                                                                                                                                                                                    |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [7:6]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                                                                                                                                                                                                                                                                      |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [10:8]  | fifo_level   | read-only   | FIFO level                                                                                                                                                                                                                                                                                                                                                                                  |
  |         |              |             |                                                                                                                                                                                                                                                                                                                                                                                             |
  |         |              |             | This field gives the number of valid bytes which are being held in the FIFO. FLEVEL = 0 when the FIFO is empty, and 16 when it is full. In memory-mapped mode and in automatic status polling mode, FLEVEL is zero.                                                                                                                                                                         |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [31:11] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                                                                                                                                                                                                                                                                      |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+





QUADSPI_FCR Register
====================

flag clear register

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CTEF", "attr":"write1c" },
    {"bits": 1, "name": "CTCF", "attr":"write1c" },
    {"bits": 1, "name": "CSMF", "attr":"write1c" },
    {"bits": 1, "name": "CTOF", "attr":"write1c" },
    {"bits": 28, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_FCR_subfields:

.. table:: QUADSPI_FCR subfeild description

  +--------+--------------+-------------+----------------------------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                                              |
  +========+==============+=============+==========================================================+
  | [0:0]  | ctef         | write1c     | Clear transfer error flag                                |
  |        |              |             |                                                          |
  |        |              |             | Writing 1 clears the TEF flag in the QUADSPI_SR register |
  +--------+--------------+-------------+----------------------------------------------------------+
  | [1:1]  | ctcf         | write1c     | Clear transfer complete flag                             |
  |        |              |             |                                                          |
  |        |              |             | Writing 1 clears the TCF flag in the QUADSPI_SR register |
  +--------+--------------+-------------+----------------------------------------------------------+
  | [2:2]  | csmf         | write1c     | Clear status match flag                                  |
  |        |              |             |                                                          |
  |        |              |             | Writing 1 clears the SMF flag in the QUADSPI_SR register |
  +--------+--------------+-------------+----------------------------------------------------------+
  | [3:3]  | ctof         | write1c     | Clear timeout flag                                       |
  |        |              |             |                                                          |
  |        |              |             | Writing 1 clears the TOF flag in the QUADSPI_SR register |
  +--------+--------------+-------------+----------------------------------------------------------+
  | [31:4] | Reserved     | read-write  | Reads will return zeros and writes will have no effect   |
  +--------+--------------+-------------+----------------------------------------------------------+





QUADSPI_DLR Register
====================

Datalength 

Number of data to be retrieved (value+1) in indirect and status-polling modes. A value no
greater than 3 (indicating 4 bytes) should be used for status-polling mode.
All 1s in indirect mode means undefined length, where QUADSPI will continue until the
end of memory, as defined by FSIZE.
0x0000_0000: 1 byte is to be transferred
0x0000_0001: 2 bytes are to be transferred
0x0000_0002: 3 bytes are to be transferred
0x0000_0003: 4 bytes are to be transferred
...
0xFFFF_FFFD: 4,294,967,294 (4G-2) bytes are to be transferred
0xFFFF_FFFE: 4,294,967,295 (4G-1) bytes are to be transferred
0xFFFF_FFFF: undefined length -- all bytes until the end of Flash memory (as defined by FSIZE) are to be transferred. Continue reading indefinitely if FSIZE = 0x1F.

DL[0] is stuck at ‘1’ in dual-flash mode (DFM = 1) even when ‘0’ is written to this bit,
thus assuring that each access transfers an even number of bytes.
This field has no effect when in memory-mapped mode (FMODE = 10).

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "QUADSPI_DLR", "attr":"read-write" }]


QUADSPI_CCR Register
====================

Communication Configuration Register

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 8, "name": "INSTRUCTION", "attr":"read-write" },
    {"bits": 2, "name": "IMODE", "attr":"read-write" },
    {"bits": 2, "name": "ADMODE", "attr":"read-write" },
    {"bits": 2, "name": "ADSIZE", "attr":"read-write" },
    {"bits": 2, "name": "ABMODE", "attr":"read-write" },
    {"bits": 2, "name": "ABSIZE", "attr":"read-write" },
    {"bits": 5, "name": "DCYC", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 2, "name": "DMODE", "attr":"read-write" },
    {"bits": 2, "name": "FMODE", "attr":"read-write" },
    {"bits": 1, "name": "SIOO", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "DDRM", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_CCR_subfields:

.. table:: QUADSPI_CCR subfeild description

  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                                                                                                               |
  +=========+==============+=============+===========================================================================================================================================================================+
  | [7:0]   | instruction  | read-write  | Instructin to be sent to the external SPI device. This can only be written to when BUSY = 0.                                                                              |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [9:8]   | imode        | read-write  | Instruction Mode                                                                                                                                                          |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field defines the instruction phase mode of operation:                                                                                                               |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | - 00: No instruction                                                                                                                                                      |
  |         |              |             | - 01: Instruction on a single line                                                                                                                                        |
  |         |              |             | - 10: Instruction on two lines                                                                                                                                            |
  |         |              |             | - 11: Instruction on four lines                                                                                                                                           |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [11:10] | admode       | read-write  | Address Mode                                                                                                                                                              |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field defines the address phase mode of operation:                                                                                                                   |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | -00: No address                                                                                                                                                           |
  |         |              |             | -01: Address on a single line                                                                                                                                             |
  |         |              |             | -10: Address on two lines                                                                                                                                                 |
  |         |              |             | -11: Address on four lines                                                                                                                                                |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [13:12] | adsize       | read-write  | Address Size                                                                                                                                                              |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This bit defines address size:                                                                                                                                            |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | - 00: 8-bit address                                                                                                                                                       |
  |         |              |             | - 01: 16-bit address                                                                                                                                                      |
  |         |              |             | - 10: 24-bit address                                                                                                                                                      |
  |         |              |             | - 11: 32-bit address                                                                                                                                                      |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [15:14] | abmode       | read-write  | Alternate bytes mode                                                                                                                                                      |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field defines the alternate-bytes phase mode of operation:                                                                                                           |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | -00: No alternate bytes                                                                                                                                                   |
  |         |              |             | -01: Alternate bytes on a single line                                                                                                                                     |
  |         |              |             | -10: Alternate bytes on two lines                                                                                                                                         |
  |         |              |             | -11: Alternate bytes on four lines                                                                                                                                        |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [17:16] | absize       | read-write  | Alternate byte ssize                                                                                                                                                      |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This bit defines alternate bytes size:                                                                                                                                    |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | - 00: 8-bit alternate byte                                                                                                                                                |
  |         |              |             | - 01: 16-bit alternate bytes                                                                                                                                              |
  |         |              |             | - 10: 24-bit alternate bytes                                                                                                                                              |
  |         |              |             | - 11: 32-bit alternate bytes                                                                                                                                              |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [22:18] | dcyc         | read-write  | Number of dummy cycles                                                                                                                                                    |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field defines the duration of the dummy phase. In both SDR and DDR modes, it specifies a number of CLK cycles (0-31).  This field can be written only when BUSY = 0. |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [23:23] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                                                    |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [25:24] | dmode        | read-write  | Data mode                                                                                                                                                                 |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field defines the data phase’s mode of operation:                                                                                                                    |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             |   - 00: No data                                                                                                                                                           |
  |         |              |             |   - 01: Data on a single line                                                                                                                                             |
  |         |              |             |   - 10: Data on two lines                                                                                                                                                 |
  |         |              |             |   - 11: Data on four lines                                                                                                                                                |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field also determines the dummy phase mode of operation.                                                                                                             |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [27:26] | fmode        | read-write  | Functional mode                                                                                                                                                           |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field defines the QUADSPI functional mode of operation.                                                                                                              |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | - 00: Indirect write mode                                                                                                                                                 |
  |         |              |             | - 01: Indirect read mode                                                                                                                                                  |
  |         |              |             | - 10: Automatic polling mode                                                                                                                                              |
  |         |              |             | - 11: Memory-mapped mode                                                                                                                                                  |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [28:28] | sioo         | read-write  | Send instruction only once mode                                                                                                                                           |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This bit has no effect when IMODE = 00.                                                                                                                                   |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | - 0: Send instruction on every transaction                                                                                                                                |
  |         |              |             | - 1: Send instruction only for the first command                                                                                                                          |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [30:29] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                                                    |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [31:31] | ddrm         | read-write  | Double data rate mode                                                                                                                                                     |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This bit sets the DDR mode for the address, alternate byte and data phase:                                                                                                |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             |   - 0: DDR Mode disabled                                                                                                                                                  |
  |         |              |             |   - 1: DDR Mode enabled                                                                                                                                                   |
  |         |              |             |                                                                                                                                                                           |
  |         |              |             | This field can be written only when BUSY = 0.                                                                                                                             |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------+





QUADSPI_AR Register
===================

Address

Address to be send to the external Flash memory
Writes to this field are ignored when BUSY = 0 or when FMODE = 11 (memory-
mapped mode).

In dual flash mode, ADDRESS[0] is automatically stuck to ‘0’ as the address
should always be even

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "QUADSPI_AR", "attr":"read-write" }]


QUADSPI_ABR Register
====================

Alternate Bytes

Optional data to be send to the external SPI device right after the address.
This field can be written only when BUSY = 0.

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "QUADSPI_ABR", "attr":"read-write" }]


QUADSPI_DR Register
===================

Data to be sent/received to/from the external SPI device.
In indirect write mode, data written to this register is stored on the FIFO before it
is sent to the Flash memory during the data phase. If the FIFO is too full, a write
operation is stalled until the FIFO has enough space to accept the amount of data
being written.

In indirect read mode, reading this register gives (via the FIFO) the data which
was received from the Flash memory. If the FIFO does not have as many bytes as
requested by the read operation and if BUSY=1, the read operation is stalled until
enough data is present or until the transfer is complete, whichever happens first.

In automatic polling mode, this register contains the last data read from the Flash
memory (without masking).

Word, halfword, and byte accesses to this register are supported. In indirect write
mode, a byte write adds 1 byte to the FIFO, a halfword write 2, and a word write
4. Similarly, in indirect read mode, a byte read removes 1 byte from the FIFO, a
halfword read 2, and a word read 4. Accesses in indirect mode must be aligned to
the bottom of this register: a byte read must read DATA[7:0] and a halfword read
must read DATA[15:0].

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "QUADSPI_DR", "attr":"read-write" }]


QUADSPI_PSMKR Register
======================

Status mask

Mask to be applied to the status bytes received in polling mode.
For bit n:

- 0: Bit n of the data received in automatic polling mode is masked and its value is not considered in the matching logic
- 1: Bit n of the data received in automatic polling mode is unmasked and its value is considered in the matching logic

This field can be written only when BUSY = 0.

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "QUADSPI_PSMKR", "attr":"read-write" }]


QUADSPI_PSMAR Register
======================

Status match

Value to be compared with the masked status register to get a match.
This field can be written only when BUSY = 0.

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "QUADSPI_PSMAR", "attr":"read-write" }]


QUADSPI_PIR Register
====================

Polling Interval register

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "INTERVAL", "attr":"read-write" },
    {"bits": 16, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_PIR_subfields:

.. table:: QUADSPI_PIR subfeild description

  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                           |
  +=========+==============+=============+=======================================================================+
  | [15:0]  | interval     | read-write  | Polling interval                                                      |
  |         |              |             |                                                                       |
  |         |              |             | Number of CLK cycles between to read during automatic polling phases. |
  |         |              |             |                                                                       |
  |         |              |             | This field can be written only when BUSY = 0.                         |
  +---------+--------------+-------------+-----------------------------------------------------------------------+
  | [31:16] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                |
  +---------+--------------+-------------+-----------------------------------------------------------------------+





QUADSPI_LPTR Register
=====================

Low power timeout register

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "TIMEOUT", "attr":"read-write" },
    {"bits": 16, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _QUADSPI_LPTR_subfields:

.. table:: QUADSPI_LPTR subfeild description

  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                   |
  +=========+==============+=============+===============================================================================+
  | [15:0]  | timeout      | read-write  | Timeout period                                                                |
  |         |              |             |                                                                               |
  |         |              |             | After each access in memory-mapped mode, the QUADSPI prefetches the           |
  |         |              |             | subsequent bytes and holds these bytes in the FIFO. This field indicates how  |
  |         |              |             | many CLK cycles the QUADSPI waits after the FIFO becomes full until it raises |
  |         |              |             | nCS, putting the Flash memory in a lower-consumption state.                   |
  |         |              |             |                                                                               |
  |         |              |             | This field can be written only when BUSY = 0.                                 |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+
  | [31:16] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                        |
  +---------+--------------+-------------+-------------------------------------------------------------------------------+





IO and Sideband Signals
=======================



The following table describes the io-signals generated from this IP that may
directly or indirectly drive certain IO pads.

.. tabularcolumns:: |l|l|l|l|

.. _QSPI_io_signals:

.. table:: QSPI generated IO signals

  ===================  ======  ===========  =======================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  =======================
  clk_o                     1  output       QSPI Clock
  io_o                      4  output       QSPI data output
  io_en                     4  output       QSPI data output-enable
  io_i                      4  input        QSPI data input
  ncs                       1  output       QSPI chip select
  ===================  ======  ===========  =======================

.. note:: some of these signals may be muxed with other functional IO from different
  ips and users should refer to any pinmux module available on chip





.. tabularcolumns:: |l|l|l|l|

.. _QSPI_sb_signals:

.. table:: QSPI generated side-band signals generated

  ===================  ======  ===========  =============================================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  =============================================================================
  interrupt                 6  output       interrupt signals from status register. The interrupts are ordered as follows

                                            - Bit-0: Timeout Out interrupt. This bit is set when timeout occurs.
                                              It is cleared by writing 1 to CTOF.
                                            - Bit-1: status match interrupt. This bit is set in automatic polling
                                              mode when the unmasked received data matches the corresponding bits
                                              in the match register (QUADSPI_PSMAR). It is cleared by writing 1 to
                                              CSMF.
                                            - Bit-2: FIFO threshold interrupt. In indirect mode, this bit is set
                                              when the FIFO threshold has been reached, or if there is any data
                                              left in the FIFO after reads from the Flash memory are complete. It
                                              is cleared automatically as soon as threshold condition is no longer
                                              true. In automatic polling mode this bit is set every time the status
                                              register is read, and the bit is cleared when the data register is
                                              read.
                                            - Bit-3: Transfer complete flag. This bit is set in indirect mode when
                                              the programmed number of data has been transferred or in any mode
                                              when the transfer has been aborted.It is cleared by writing 1 to CTCF.
                                            - Bit-4: Transfer error flag. This bit is set in indirect mode when an
                                              invalid address is being accessed in indirect mode. It is cleared by
                                              writing 1 to CTEF.
                                            - Bit-5: indicates the request is ready
  ===================  ======  ===========  =============================================================================



Direct Memory Region
====================

Following is the list of direct memory regions available in the current
instance:

- (0x90000000:0x9FFFFFFF)[Read-Only]
