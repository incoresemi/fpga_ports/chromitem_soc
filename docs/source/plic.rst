.. _plic:

##########################################
Platform Level Interrupt Controller (PLIC)
##########################################

.. include:: <isonum.txt>

This chapter discusses the operation of the Platform Level Interrupt Controller (PLIC)
instantiated in this design. The PLIC is fully compliant with the RISC-V PLIC spec and
*The RISC-V Instruction Set Manual, Volume II: Privileged Architecture, Version 1.11*.


IP Details and Available Configuration
======================================


:numref:`PLIC_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _PLIC_ip_details:

.. table:: PLIC IP details

  ========================================  ==============
  ..                                        **Value**
  ========================================  ==============
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.1.2
  Ip Type                                   memory_mapped
  Numer of Config Registers                 5
  Direct Memory Region                      None
  Configuration Register Alignment (bytes)  4
  ========================================  ==============

:numref:`PLIC_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _PLIC_configuration_details:

.. table:: PLIC IP Configuration Options

  ===============  ========================  ======================================================================================================================================================================================
  Configuration    Options                   Description
  ===============  ========================  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L, AXI4          Choice of bus interface protocol supported on this IP
  Base address     Integer                   The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X3FFFFFF      The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  sources          Integer > 1 and  < 1024   Number of interrupt sources handled by the PLIC. Value <= 1024
  targets          Integer > 1 and  < 15872  Number of targets/contexts the PLIC can support. Value <= 15872
  maxpriority      Integer > 1               The Maximum priority that an interrupt source can be assigned. Value >= 1
  ===============  ========================  ======================================================================================================================================================================================



PLIC Instance Details
=====================



:numref:`PLIC_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _PLIC_instance_details:

.. table:: PLIC Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0XC000000
  Bound Address         0XFFFFFFF
  Bytes reserved        0X3FFFFFF
  Bus Interface         AXI4L
  sources               0X21
  targets               0X1
  maxpriority           0X3
  ====================  ====================


Register Map
============



The register map for the PLIC control registers is shown in
:numref:`PLIC_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _PLIC_register_map:

.. table:: PLIC Register Mapping for all configuration registes

  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | Register-Name      | Offset(hex)   | Size(Bits)   | Reset(hex)   | Description                                                                        |
  +====================+===============+==============+==============+====================================================================================+
  | source_priority-0  | 0X0           | 24           | 0X0          | Regsiter holds the priority value of the respective interrupt source.              |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | source_priority-1  | 0X4           | 24           | 0X0          |                                                                                    |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | \.\.\.\.           | \.\.\.\.      | \.\.\.\.     | \.\.\.\.     |                                                                                    |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | source_priority-32 | 0X80          | 24           | 0X0          |                                                                                    |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | source_pending     | 0X1000        | 32           | 0X0          | Register holds the pending interrupt bits for upto 32 sources in a single register |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | target_enables-0   | 0X2000        | 32           | 0X0          | Register holds the interrupt enable bits for upto 32 sources in a single register  |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | target_enables-1   | 0X2004        | 32           | 0X0          |                                                                                    |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | target_threshold   | 0X200000      | 32           | 0X0          | Register holds the priority threshold of the respective target                     |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+
  | target_claim       | 0X200004      | 32           | 0X0          | Register holds interrupt claim/completion information for the respective target    |
  +--------------------+---------------+--------------+--------------+------------------------------------------------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface





The register access attributes for the PLIC control registers are shown in 
:numref:`PLIC_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _PLIC_register_access_attr:

.. table:: PLIC Register Access Attributes for all configuration Registers

  ================  =============  ============  ============  ============
  Register-Name     Access Type    Reset Type    Min Access    Max Access
  ================  =============  ============  ============  ============
  source_priority   read-write     synchronous   1B            4B
  source_pending    read-only      synchronous   1B            4B
  target_enables    read-write     synchronous   1B            4B
  target_threshold  read-write     synchronous   1B            4B
  target_claim      read-write     synchronous   1B            4B
  ================  =============  ============  ============  ============

:numref:`PLIC_register_sideeffects` captures the side-effects caused to either reads or writes
on certain registers.

.. tabularcolumns:: |l|l|l|

.. _PLIC_register_sideeffects:

.. table:: PLIC Register Side Effects

  ===============  ==================================================================================================================================================================  ====================
  Register-Name    Read Side Effects                                                                                                                                                   Write Side Effects
  ===============  ==================================================================================================================================================================  ====================
  target_claim     a read tries to claim the ID of the highest interrupt pending. A successfull claim will automatically clear the corresponding pending bit in the interrupt source.  none
  ===============  ==================================================================================================================================================================  ====================

.. note:: Registers not included in the Side Effects table have no side-effects generated
  either on a read or a write.





PLIC Interrupt Priorities
=========================
The PLIC supports interrupt priorities, i.e. each PLIC interrupt source can be assigned a 
priority by writing to its memory-mapped ``source_priority`` register. A priority value of 0 
is reserved to mean *never interrupt* and effectively disables the interrupt. Priority 1 is 
the lowest active priority while the maximun level of priority is defined by the ``maxpriority``
parameter defined in :numref:`PLIC_instance_details`. Ties between global interrupts of the 
same priority are broken by the Interrupt ID; interrupts with the lowest ID have the highest 
effective priority.

The base address of each ``source_priority`` register is calculated as: ``Base Address + 4`` |times| ``Interrupt ID``

Following is the generic layout of the ``source_priority`` registers.

.. _plic_source_priority_layout:

.. table:: PLIC Interrupt Priority Register Layout (``source_priority``) [PrSize = Log(``maxpriority``)]

  ============ ========== ========= ===== ================================================
  Bits         Field Name Attribute Reset Description
  ============ ========== ========= ===== ================================================
  [PrSize-1:0] Priority   RW        0     Sets the priority for a given global interrupt
  [31:PrSize]  Reserved   RW        0     Writes have no effect and reads will always be 0  
  ============ ========== ========= ===== ================================================


PLIC Interrupt Pending Bits
===========================

The current status of the interrupt source pending bits in the PLIC core can be read from the 
pending array, organized as 32-bit register. The pending bit for interrupt ID ``N`` is stored in 
bit ``(N mod 32)`` of word ``(N/32)``. Bit 0 of word 0, which represents the non-existent 
interrupt source 0, is hardwired to zero.
A pending bit in the PLIC core can be cleared by setting the associated enable bit then 
performing a claim.

The ``source_pending`` registers start at: ``Base Address + 0x1000``

Following is the layout of the first ``source_pending`` register.

.. _plic_source_pending_layout:

.. table:: First PLIC Interrupt Pending Register Layout (``source_pending``)

  ============ ==================== ========= ===== ================================================
  Bits         Field Name           Attribute Reset Description
  ============ ==================== ========= ===== ================================================
  0            Interrupt-0 Pending  RW        0     Reserved
  1            Interrupt-1 Pending  RW        0     Pending bit for global interrupt 1
  2            Interrupt-2 Pending  RW        0     Pending bit for global interrupt 2
        \.\.\.\.\.
  --------------------------------------------------------------------------------------------------
  31           Interrupt-31 Pending RW        0     Pending bit for global interrupt 31
  ============ ==================== ========= ===== ================================================

.. note:: If ``sources`` < 31 then bits [31:sources] in the above register are reserved where writes
   have no effect and reads will always return zeros. 

Following is the layout of the first ``source_pending`` register.

.. _plic_source_pending_layout_2:

.. table:: Second PLIC Interrupt Pending Register Layout (``source_pending``). 

  ============ ========================= ========= ===== ================================================
  Bits         Field Name                Attribute Reset Description
  ============ ========================= ========= ===== ================================================
  0            Interrupt-32 Pending      RW        0     Reserved
        \.\.\.\.\.
  -------------------------------------------------------------------------------------------------------
  sources      Interrupt-sources Pending RW        0     Pending bit for global interrupt: ``sources``
  [31:sources] Reserved                  RW        0     Writes have no effect and reads will always be 0
  ============ ========================= ========= ===== ================================================
   
.. note:: The above register is available only if ``sources`` > 31


Interrupt Enables
=================
Each global interrupt can be enabled by setting the corresponding bit in the enables register. 
The enables registers are accessed as a contiguous array of 32-bit registers, packed the same way 
as the pending bits. Bit 0 of enable register 0 represents the non-existent interrupt ID 0 and 
is hardwired to 0. This instance has `targets` number of contexts supported.
The layout and access attributes for the `target_enables` registers is analogous to the
`source_pending` regsiters.

Interrupt Thresholds
====================
PLIC provides context based threshold register for the settings of the interrupt priority threshold 
of each context. The threshold register is a read-write register. The PLIC will mask all PLIC interrupts of 
a priority less than or equal to threshold. For example, a `threshold` value of zero permits all 
interrupts with non-zero priority.

The Threshold registers are located at 4K alignement starting from offset 0x200000.

Following is the generic layout of the *target_threshold* registers.

.. _plic_target_threshold_layout:

.. table:: PLIC Threshold Register Layout (``target_threshold``) [ThSize = Log(``maxpriority``)]

  ============ ========== ========= ===== ================================================
  Bits         Field Name Attribute Reset Description
  ============ ========== ========= ===== ================================================
  [ThSize-1:0] Priority   RW        0     Sets the threshold for a given global interrupt
  [31:ThSize]  Reserved   RW        0     Writes have no effect and reads will always be 0  
  ============ ========== ========= ===== ================================================


Interrupt Claim Process
=======================

The PLIC can perform an interrupt claim by reading the claim/complete register, which returns the 
ID of the highest priority pending interrupt, or zero if there are no pending interrupts. A successful 
claim will also atomically clear the corresponding pending bit on the interrupt source.

The PLIC can perform a claim at any time and the claim operation is not affected by the setting 
of the priority threshold register.

The Interrupt Claim Process register is context based and is located at (4K alignement + 4)
starts from offset 0x200000.

Interrupt Completion
====================
The PLIC signals that it has completed executing an interrupt handler by writing the interrupt ID it 
received from the claim to the claim/complete register. The PLIC does not check whether the 
completion ID is the same as the last claim ID for that target. If the completion ID does not match 
an interrupt source that is currently enabled for the target, the completion is silently ignored.

The Interrupt Completion registers are context based and are located at the same address with Interrupt 
Claim Process register, which is at (4K alignement + 4) starts from offset 0x200000.

IO and Sideband Signals
=======================





.. tabularcolumns:: |l|l|l|l|

.. _PLIC_sb_signals:

.. table:: PLIC generated side-band signals generated

  ===================  ======  ===========  ===================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ===================================================
  sb_to_targets             1  output       interrupt pending indication to respective contexts
  sb_frm_sources           33  input        interrupts arriving from various sources
  ===================  ======  ===========  ===================================================


.. include:: plic_irq_mapping.rst
