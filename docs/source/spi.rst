.. _spi:

#########################################
Serial Peripheral Interface (SPI) Module
#########################################

.. include:: <isonum.txt>

This chapter will discuss the operation of the Serial Peripheral Interface (SPI) module instantiated in this design.

IP Details and Available Configuration
======================================


:numref:`SPI_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _SPI_ip_details:

.. table:: SPI IP details

  ========================================  ==============
  ..                                        **Value**
  ========================================  ==============
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.4.0
  Ip Type                                   memory_mapped
  Numer of Config Registers                 12
  Direct Memory Region                      None
  Configuration Register Alignment (bytes)  4
  ========================================  ==============

:numref:`SPI_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _SPI_configuration_details:

.. table:: SPI IP Configuration Options

  ===============  ======================  ======================================================================================================================================================================================
  Configuration    Options                 Description
  ===============  ======================  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L, AXI4        Choice of bus interface protocol supported on this IP
  Base address     Integer                 The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X30         The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  periph_count     Integer > 0 and  < 8    An integer indicating number of peripherals that can be controlled by SPI module.
  fifo_depth       Integer > 2 and  < 256  An integer indicating depth of FIFO used by tx_fifo and rx_fifo.
  ===============  ======================  ======================================================================================================================================================================================



This IP adheres to the redefined SPI Signal names

.. list-table:: SPI Signal Terminologies
   :widths: 50 50
   :header-rows: 1

   * - Discontinued Name
     - Redefined Name
   * - Master
     - Controller
   * - Slave
     - Peripheral
   * - MISO
     - CIPO
   * - MOSI
     - COPI
   * - Slave Select
     - Chip Select / Peripheral Select

SPI Instance Details
=====================



:numref:`SPI_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _SPI_instance_details:

.. table:: SPI Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0X20100
  Bound Address         0X201FF
  Bytes reserved        0XFF
  Bus Interface         APB
  periph_count          0X1
  fifo_depth            0X14
  ====================  ====================


SPI Features
=============

The SPI module implements the standard SPI communication as controller & peripheral configuration supporting:

* Custom Setup / Hold / Inter-Transfer delays in controller mode
* 16-bit Prescalar for selecting Baudrate in controller mode
* CRC based Hashing for Rx/Tx Data
* Bit-controlled data transfer
* Interrupt line that gets connected to PLIC.

 :numref:`spi_device` shows Highlevel Architecture, Different Modes used to sample data and
 the peripheral support delays provided by the SPI modules.

.. _spi_device:

.. figure:: spi-registers.svg
   :align: center

   SPI Architecture.

The following parameters can be configured in software:

* Clock Phase and Polarity using ``CPHA`` && ``CPOL``
* Communication mode using ``DPLX``
* Setup, Hold & Inter Transfer delays using ``SPI_DR`` Registers (in Controller mode)
* CRC of Transmitted and Received Data
* Interrupt to write AND/OR Read from TxR and RxR respectively.
* Bit wise control of data Tx/Rx.

The following parameters can be configured in the hardware:

* Number of peripherals that can be controlled using the SPI module (``periph_count``)
* Depth of FIFO can be chosen (``fifo_depth``)


Register Map
============



The register map for the SPI control registers is shown in
:numref:`SPI_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _SPI_register_map:

.. table:: SPI Register Mapping for all configuration registes

  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | Register-Name   | Offset(hex)   |   Size(Bits) | Reset(hex)   | Description                                                                                       |
  +=================+===============+==============+==============+===================================================================================================+
  | SPI_CR1         | 0X0           |           32 | 0X4          | Various fields to control SPI parameters                                                          |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_CR2         | 0X4           |           32 | 0X0          | Various fields to control secondary SPI parameters                                                |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_EN          | 0X8           |            8 | 0X0          | Master control for SPI module                                                                     |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_SR          | 0XC           |           32 | 0X0          | Various fields that indicate status of SPI module.                                                |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | TXR             | 0X10          |           32 | 0X0          | Register to Holds data that needs to be sent over SPI                                             |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | RXR             | 0X14          |           32 | 0X0          | Register to Hold data that is received over the SPI                                               |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | RX_CRC          | 0X18          |           16 | 0X0          | Register to store so far received data's CRC (only on CRC_EN)                                     |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | TX_CRC          | 0X1C          |           16 | 0X0          | Register to store so far transmitted data's CRC (only on CRC_EN).                                 |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_DR          | 0X20          |           32 | 0X0          | Register to control delay parameters to support slow peripherals. ( **In controller mode only** ) |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_PSCR        | 0X24          |           16 | 0X4          | Prescalar value that divides bus clock and provides `sclk` (In controller mode only)              |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_CRCPR       | 0X28          |           16 | 0X0          | CRC Polynomial to use with CRC module                                                             |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+
  | SPI_CRCINOUT    | 0X2C          |           32 | 0X0          | CRC Initial Value & Xor Out to use with CRC module                                                |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface





The register access attributes for the SPI control registers are shown in 
:numref:`SPI_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _SPI_register_access_attr:

.. table:: SPI Register Access Attributes for all configuration Registers

  ===============  =============  ============  ============  ============
  Register-Name    Access Type    Reset Type    Min Access    Max Access
  ===============  =============  ============  ============  ============
  SPI_CR1          read-write     synchronous   4B            4B
  SPI_CR2          read-write     synchronous   4B            4B
  SPI_EN           read-write     synchronous   1B            4B
  SPI_SR           read-only      synchronous   4B            4B
  TXR              read-write     synchronous   4B            4B
  RXR              read-only      synchronous   4B            4B
  RX_CRC           read-only      synchronous   2B            4B
  TX_CRC           read-only      synchronous   2B            4B
  SPI_DR           read-write     synchronous   4B            4B
  SPI_PSCR         read-write     synchronous   2B            4B
  SPI_CRCPR        read-write     synchronous   2B            4B
  SPI_CRCINOUT     read-write     synchronous   4B            4B
  ===============  =============  ============  ============  ============




SPI_CR1 Register
================

This is a 32-bit register which allows primary controls of SPI module
including Phase, Polarity, controller / peripheral modes,  data format,
communication modes, Bits to transmit/receive.

.. list-table :: Communication modes by SPI
    :widths: 35 65
    :header-rows: 1

    * - Communication Mode
      - Configuration
    * - Duplex Mode (Tx & Rx)
      - Set ``dplx`` = 1 and transaction continues for MAX(``tx_bits``, ``rx_bits``)
    * - Simplex Mode (Tx Only)
      - Set ``dplx`` = 0 , ``rx_bits`` = 0 . Transaction continues for ``tx_bits``
    * - Simplex Mode (Rx Only)
      - Set `dplx`` = 0 , ``tx_bits`` = 0 . Transaction continues for ``rx_bits``
    * - Half Duplex Mode (First Tx then Rx)
      - Set ``dplx`` = 0 , ``is_rx_first`` = 0 . Transaction continues for SUM( ``tx_bits``, ``rx_bits`` )
    * - Half Duplex Mode (First Rx then Tx)
      - Set ``dplx`` = 0 , ``is_rx_first`` = 1 . Transaction continues for SUM( ``rx_bits``, ``tx_bits`` )

.. warning :: On choosing unlimited transactions using ( ``tx_bits`` = 255 or ``rx_bits`` = 255),
    User should be aware that:

    * In Duplex transactions, if either **rx_fifo is full** or **tx_fifo is empty**, spi will terminate
    * In Other transactions,
        #. During Rx phase, if **rx_fifo is full**, spi will terminate
        #. During Tx phase, if **tx_fifo is empty**, spi will terminate

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CPHA", "attr":"read-write" },
    {"bits": 1, "name": "CPOL", "attr":"read-write" },
    {"bits": 1, "name": "CTRLER", "attr":"read-write" },
    {"bits": 3, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "IS_RX_FIRST", "attr":"read-write" },
    {"bits": 1, "name": "LSB_1ST", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "CLR_STATE", "attr":"read-write" },
    {"bits": 1, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "CRC_LEN", "attr":"read-write" },
    {"bits": 1, "name": "CRC_EN", "attr":"read-write" },
    {"bits": 1, "name": "DPLX", "attr":"read-write" },
    {"bits": 1, "name": "SPI_DIS_SYNC", "attr":"read-write" },
    {"bits": 8, "name": "TX_BITS", "attr":"read-write" },
    {"bits": 8, "name": "RX_BITS", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CR1_subfields:

.. table:: SPI_CR1 subfeild description

  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                                           |
  +=========+==============+=============+=======================================================================================================+
  | [0:0]   | cpha         | read-write  | Selects clock phase for data capture                                                                  |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [1:1]   | cpol         | read-write  | Selects clock polarity for data capture                                                               |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [2:2]   | ctrler       | read-write  | Selects SPI Operational mode as Controller(1) / peripheral (0). Default is Controller mode.           |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [5:3]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [6:6]   | is_rx_first  | read-write  | During a Half-duplex transaction, this bit decides whether RX(1) happens first or TX(0) happens first |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [7:7]   | lsb_1st      | read-write  | Bit transfer in LSB(1) / MSB(0) First                                                                 |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [9:8]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [10:10] | clr_state    | read-write  | Clears the internal FIFO and resets Tx/Rx States. This bit will be unset on clearing internal states. |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [11:11] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [12:12] | crc_len      | read-write  | CRC Length 8-bit (0) / 16-bit (1)                                                                     |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [13:13] | crc_en       | read-write  | CRC Check Enable                                                                                      |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [14:14] | dplx         | read-write  | Selects mode of Communication Duplex (1) / Simplex (0)                                                |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [15:15] | spi_dis_sync | read-write  | | If enabled (1), when ``SPI_EN`` is written with 0, the disable will                                 |
  |         |              |             |   happen after the existing 'byte' has been transferred/received.                                     |
  |         |              |             | | If disabled (0), when ``SPI_EN`` is written with 0, the disable will                                |
  |         |              |             |   happen immediately                                                                                  |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [23:16] | tx_bits      | read-write  | to transmit (1-254) for bit transmission, 255 for unlimited data transfer                             |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+
  | [31:24] | rx_bits      | read-write  | to receive (1-254) for bit reception, 255 for unlimited data receive.                                 |
  +---------+--------------+-------------+-------------------------------------------------------------------------------------------------------+





SPI_CR2 Register
================

This is a 32-bit register which allows secondary controls of SPI module including Interrupt Controls, Idle pulse states & CRC controls.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "CS_PULS", "attr":"read-write" },
    {"bits": 1, "name": "IDLE_OUT", "attr":"read-write" },
    {"bits": 1, "name": "RX_TH_IE", "attr":"read-write" },
    {"bits": 1, "name": "ERR_IE", "attr":"read-write" },
    {"bits": 1, "name": "RXNE_IE", "attr":"read-write" },
    {"bits": 1, "name": "TXE_IE", "attr":"read-write" },
    {"bits": 4, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "FIFO_RX_TH", "attr":"read-write" },
    {"bits": 3, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "CRC_RIN", "attr":"read-write" },
    {"bits": 1, "name": "CRC_ROUT", "attr":"read-write" },
    {"bits": 6, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 3, "name": "PERIPH_ID", "attr":"read-write" },
    {"bits": 5, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CR2_subfields:

.. table:: SPI_CR2 subfeild description

  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                                                                           |
  +=========+==============+=============+=======================================================================================================================================+
  | [1:0]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [2:2]   | cs_puls      | read-write  | CS Goes High [1] or Keeps Low [0] during inter-transfer delay (See SPI_DR[3]). (Applicable in Controller mode (SPI_CR1[2] == 1) only) |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [3:3]   | idle_out     | read-write  | Keeps Output line High [1] or Low [0] during (Half)duplex transaction's idle phase.                                                   |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [4:4]   | rx_th_ie     | read-write  | Enable Interrupt when RX FIFO reaches the threshold specified in ``fifo_rx_th``                                                       |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [5:5]   | err_ie       | read-write  | Enable Interrupt for Errors                                                                                                           |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [6:6]   | rxne_ie      | read-write  | Enable Interrupt when RxR is NOT empty                                                                                                |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [7:7]   | txe_ie       | read-write  | Enable Interrupt when TxR is empty                                                                                                    |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [11:8]  | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [12:12] | fifo_rx_th   | read-write  | Sets Threshold for RX FIFO at Half(0) or  3/4th (1). This threshold is used to generate threshold interrupt ``rx_th_ie``              |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [15:13] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [16:16] | crc_rin      | read-write  | Reflect the data fed for CRC - www.crccalc.com                                                                                        |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [17:17] | crc_rout     | read-write  | Reflect the CRC output - www.crccalc.com                                                                                              |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [23:18] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [26:24] | periph_id    | read-write  | Chip Selector (NCS_P) where P=0 to 7 (depends on the ``periph_count`` in IP)                                                          |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+
  | [31:27] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                                                                                |
  +---------+--------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------+





SPI_EN Register
===============

8-bit register that acts as control bit for the SPI module.
On completion of transaction this bit will be unset by SPI module.
.. note :: Read all the warnings and notes in the ``TXR`` and ``RXR`` registers before
   using this register

.. bitfield::
    :bits: 8
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "ENABLE", "attr":"read-write" },
    {"bits": 7, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_EN_subfields:

.. table:: SPI_EN subfeild description

  +--------+--------------+-------------+--------------------------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                                            |
  +========+==============+=============+========================================================+
  | [0:0]  | enable       | read-write  | Enables (1) / Disables (0) the SPI module              |
  +--------+--------------+-------------+--------------------------------------------------------+
  | [7:1]  | Reserved     | read-write  | Reads will return zeros and writes will have no effect |
  +--------+--------------+-------------+--------------------------------------------------------+





SPI_SR Register
===============

32-bit register that indicates status of SPI module, including the status to write/read TxR/RxR respectively along with internal FIFO status.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "RXNE", "attr":"read-only" },
    {"bits": 1, "name": "TXE", "attr":"read-only" },
    {"bits": 1, "name": "RXF", "attr":"read-only" },
    {"bits": 1, "name": "TXNF", "attr":"read-only" },
    {"bits": 6, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "OVRF", "attr":"read-only" },
    {"bits": 1, "name": "BSY", "attr":"read-only" },
    {"bits": 4, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 8, "name": "FTCNT", "attr":"read-only" },
    {"bits": 8, "name": "FRCNT", "attr":"read-only" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_SR_subfields:

.. table:: SPI_SR subfeild description

  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                                       |
  +=========+==============+=============+===================================================================================+
  | [0:0]   | rxne         | read-only   | RxR is not empty (when set, Ready to read RxR)                                    |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [1:1]   | txe          | read-only   | TxR is empty (when set, Ready in writing to TxR)                                  |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [2:2]   | rxf          | read-only   | RxFIFO is full                                                                    |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [3:3]   | txnf         | read-only   | TxFIFO is not full (Data from TxR will be enqueued as long as this field is set ) |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [9:4]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect                            |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [10:10] | ovrf         | read-only   | Overflow Flag (Tx FIFO overflow)                                                  |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [11:11] | bsy          | read-only   | SPI is performing Transactions (1) / IDLE (0)                                     |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [15:12] | Reserved     | read-write  | Reads will return zeros and writes will have no effect                            |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [23:16] | ftcnt        | read-only   | Yet- to -transmit bytes in Tx FIFO ( Indicates number of bytes filled in FIFO)    |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+
  | [31:24] | frcnt        | read-only   | Unread bytes in Rx FIFO ( Indicates number of bytes filled in FIFO)               |
  +---------+--------------+-------------+-----------------------------------------------------------------------------------+





TXR Register
============

| 32-bit register that should be written for data transmission.
| Data should be written to this register when `txe` is set and each write is aligned
  according to the ``lsb_1st``.
| As long as ``txnf`` is set, data can be written into this register that will be queued
  into TxFIFO whose filled status can be found from ``ftcnt``.

.. note :: This register can be written even during ``SPI_EN`` is disabled. This allows
  Tx Data to be written to FIFO then enable the SPI.

.. note :: | If the selected communication mode involves 'Tx', the transaction will start
             depending on the data filled into the tx_fifo.
           | If ``txbits`` > 0 and ``txbits`` < ``fifo_depth x 8`` , transaction will
             only start after tx_fifo gets filled.
           | Otherwise the tx_fifo should be filled to half of the ``fifo_depth`` to
             start the transaction.

.. note :: Total Tx capacity = ``fifo_depth`` + 4 ( ``TXR`` ) bytes and the ``ftcnt``
  only shows the number of bytes held in ``tx_fifo``

.. warning :: | When a new Transaction is being started, checking contents of ``ftcnt``
      ``frcnt`` is a good practice to avoid stale data from previous transactions.
    | If any of the field returns non-zero, Registers ``RXR``,``TXR`` should be read
      to discard and FIFO can be cleared by ``clr_state`` bit.

.. note :: It is better practice to fill the data into fifo and then assert ``SPI_EN`` = 1

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "TXR", "attr":"read-write" }]


RXR Register
============

| 32-bit Read-Only register that should be read during data reception.
| Data should be read from this register when ``rxne`` is set and each read is aligned
  according to the ``lsb_1st``.
| It is best practice to keep track of ``frcnt`` to know how many bytes are yet to be
  read from internal RxFIFO.
| If the FIFO fills up `rxf` will be set (ending the receive transaction)

.. note :: This register can be read even after ``SPI_EN`` is disabled. This allows
  Rx Data to be read from FIFO after transaction completes.

.. note :: Total Rx capacity = ``fifo_depth`` + 4 ( ``RXR`` ) bytes and the ``frcnt``
  only shows the number of bytes held in ``rx_fifo``.

.. warning :: | The first 4 bytes received during a transaction will be pushed into RXR,
  | which makes ``frcnt=0``  although 4 bytes were received. from 5th byte the ``frcnt``
    will increment.

.. warning:: Contents of FIFO will get cleared on setting ``clr_state`` bit.

.. bitfield::
    :bits: 32
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 32, "name": "RXR", "attr":"read-only" }]


RX_CRC Register
===============

| 16-bit register that'll be updated with CRC hash of data received. The Hash will
  depend on ``crc_poly``, ``crc_init``, ``crc_xor``, ``crc_en``, ``crc_len``,
| ``crc_rin`` & ``crc_rout`` parameters. Intermediate hashes are stored into this
  register periodically. Final hash can be read from this register when transaction
  winds up.

.. note :: The CRC is calculated whenever ``RX_FIFO`` is enqueued. It should be noted
  that the data stored in ``RX_FIFO`` is passed into ``RXR``

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "RXCRC", "attr":"read-only" }]




.. tabularcolumns:: |l|l|l|l|

.. _RX_CRC_subfields:

.. table:: RX_CRC subfeild description

  +--------+--------------+-------------+---------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                           |
  +========+==============+=============+=======================================+
  | [15:0] | rxcrc        | read-only   | CRC Hash for data received during Rxn |
  +--------+--------------+-------------+---------------------------------------+





TX_CRC Register
===============

| 16-bit register that'll be updated with CRC hash of data transmitted. The Hash will
  depend on ``crc_poly``, ``crc_init``, ``crc_xor``, ``crc_en``, ``crc_len``,
| ``crc_rin`` & ``crc_rout`` parameters. Intermediate hashes are stored into this
  register periodically. Final hash can be read from this register when transaction
  winds up.

.. note :: The CRC is calculated whenever ``TX_FIFO`` is enqueued. It should be noted
  that the data written into ``TXR`` is enqueued into ``TX_FIFO``.

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "TXCRC", "attr":"read-only" }]




.. tabularcolumns:: |l|l|l|l|

.. _TX_CRC_subfields:

.. table:: TX_CRC subfeild description

  +--------+--------------+-------------+---------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                           |
  +========+==============+=============+=======================================+
  | [15:0] | txcrc        | read-only   | CRC Hash for data received during Txn |
  +--------+--------------+-------------+---------------------------------------+





SPI_DR Register
===============

| This 32-bit register holds 'additional' delay that should be provided
  to support slow-responding peripherals, whose 'setup', 'hold' & 'xfer' delay
  should be assisted by controller.
| This register provides option of choosing clock source and the delay
  enabling bits for quick enable and disable of particular delay.

The mathematical relationship of Abs. Delay in terms of **bus clock** is
provided below.

.. list-table:: Delays in various
     :widths: 25 25 25 25
     :header-rows: 1

     * - condition
       - Setup
       - Hold
       - Xfer
     * - \*_EN is 0
       - 2
       - 3
       - 3
     * - | \*_EN is 1
         | clk_src = 0
       - Refer Equation :eq:`sh0delay`
       - Refer Equation :eq:`sh0delay`
       - Refer Equation :eq:`xf0delay`
     * - | \*_EN is 1 \
         | clk_src = 1
       - Refer Equation :eq:`sh1delay`
       - Refer Equation :eq:`sh1delay`
       - Refer Equation :eq:`xf1delay`

.. note ::
  | When using Equation :eq:`sh0delay` or :eq:`sh1delay` for calculating HOLD\_DELAY
    replace the SETUP\_DELAY with HOLD\_DELAY appropriately.
  | These equations are valid only when specified delays are non-zero values

Use ``cpol`` value specified in the SPI_CR1[2] bit in the below equations.

  Setup / Hold Delay,
    when ``clk_src = 0`` , Delay is represented as

      .. math:: Abs. delay = SETUP\_DELAY + 4
         :label: sh0delay

    | when ``clk_src = 1`` and :math:`SETUP\_DELAY \neq 0`
    | Abs. Delay is represented as

      .. math:: Abs. delay = ( SETUP\_DELAY \times prescalar ) + 4 - \left((1 - \mathbf{cpol} ) \times \frac{prescalar}{2}\right)
         :label: sh1delay

  Xfer Delay ,
    when ``clk_src = 0`` , Delay is represented as

      .. math:: Abs. delay = XFER\_DELAY + 5
         :label: xf0delay

    | when ``clk_src = 1`` and :math:`XFER\_DELAY \neq 0`
    | Abs. Delay is represented as

      .. math:: Abs. delay = ( XFER\_DELAY \times prescalar ) + 5 - \left((1 - \mathbf{cpol} ) \times \frac{prescalar}{2}\right)
         :label: xf1delay

  .. note:: During the xfer delay, ncs follows the ``cs_puls``, and
          xfer delay is valid only for Half-duplex Transactions.

  .. note:: By default, when all delays are disabled, there's implicit setup ,
         xfer and hold Abs. delays of 2 , 3 \& 3 bus clocks respectively.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "CLK_SRC", "attr":"read-write" },
    {"bits": 1, "name": "SD_EN", "attr":"read-write" },
    {"bits": 1, "name": "HD_EN", "attr":"read-write" },
    {"bits": 1, "name": "XD_EN", "attr":"read-write" },
    {"bits": 4, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 8, "name": "SETUP_DELAY", "attr":"read-write" },
    {"bits": 8, "name": "HOLD_DELAY", "attr":"read-write" },
    {"bits": 8, "name": "XFER_DELAY", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_DR_subfields:

.. table:: SPI_DR subfeild description

  +---------+--------------+-------------+----------------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                          |
  +=========+==============+=============+======================================================================+
  | [0:0]   | clk_src      | read-write  | Selects bus clock(0) / SPI clock(1) for delays                       |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [1:1]   | sd_en        | read-write  | Enable Setup delay                                                   |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [2:2]   | hd_en        | read-write  | Enable Hold Delay                                                    |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [3:3]   | xd_en        | read-write  | Enable Transfer Delay (Applicable for Half Duplex transactions only) |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [7:4]   | Reserved     | read-write  | Reads will return zeros and writes will have no effect               |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [15:8]  | setup_delay  | read-write  | 8 bit delay for setup time                                           |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [23:16] | hold_delay   | read-write  | 8-bit delay for Hold time                                            |
  +---------+--------------+-------------+----------------------------------------------------------------------+
  | [31:24] | xfer_delay   | read-write  | 8-bit delay between transactions (idles to CS_PULS)                  |
  +---------+--------------+-------------+----------------------------------------------------------------------+





SPI_PSCR Register
=================

The prescalar value is the divisor value for clock. Prescalar minimum value is 2 and maximum value is 65534.

.. note:: Prescalar should be an even value in ``controller`` mode. if odd value is sent, it will be added
          '+1' to make it even. This field has no significance in ``peripheral`` mode.

.. warning:: In ``Controller`` mode, recommended Minimum value is 4. When Using prescalar
             value of 2, the SPI runs at half the frequency of bus clock. Hence, during
             transactions intermediate delays might be observable.

.. warning:: When the SPI Module is operated in ``peripheral mode``, the Max. frequency
             of clock driving ``sclk`` should be
             :math:`\mathbf{max\_sclk\_frequency} \leq \frac{\mathbf{bus\_clock\_frequency}}{4}`

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "PRESCALAR", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_PSCR_subfields:

.. table:: SPI_PSCR subfeild description

  +--------+--------------+-------------+-----------------------------------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                                                     |
  +========+==============+=============+=================================================================+
  | [15:0] | prescalar    | read-write  | value > 1 and  < 65535 are valid values for dividing bus clock. |
  +--------+--------------+-------------+-----------------------------------------------------------------+





SPI_CRCPR Register
==================

This 16-bit register holds the value of polynomial
according to the `crc_len` that dictates between CRC8 / CRC16

.. bitfield::
    :bits: 16
    :lanes: 2
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "CRC_POLY", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CRCPR_subfields:

.. table:: SPI_CRCPR subfeild description

  +--------+--------------+-------------+-----------------------------------------------+
  | Bits   | Field Name   | Attribute   | Description                                   |
  +========+==============+=============+===============================================+
  | [15:0] | crc_poly     | read-write  | Polynomial to be used for generating CRC Hash |
  +--------+--------------+-------------+-----------------------------------------------+





SPI_CRCINOUT Register
=====================

This 32-bit register holds the value of initial and Output XOR values
according to the `crc_len` that dictates between CRC8 / CRC16

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "CRC_INIT", "attr":"read-write" },
    {"bits": 16, "name": "CRC_XOR_OUT", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _SPI_CRCINOUT_subfields:

.. table:: SPI_CRCINOUT subfeild description

  +---------+--------------+-------------+-----------------------------------------------------------------+
  | Bits    | Field Name   | Attribute   | Description                                                     |
  +=========+==============+=============+=================================================================+
  | [15:0]  | crc_init     | read-write  | Initial value of CRC Algo. that is used to generate Hash.       |
  +---------+--------------+-------------+-----------------------------------------------------------------+
  | [31:16] | crc_xor_out  | read-write  | Value with which final output should be XORed - www.crccalc.com |
  +---------+--------------+-------------+-----------------------------------------------------------------+





IO and Sideband Signals
=======================



The following table describes the io-signals generated from this IP that may
directly or indirectly drive certain IO pads.

.. tabularcolumns:: |l|l|l|l|

.. _SPI_io_signals:

.. table:: SPI generated IO signals

  ===================  ======  ===========  ====================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ====================================================
  copi_in                   1  input        Peripheral Input
  copi_out_en               1  output       Tristate control Signal OE=1
  copi_out                  1  output       Controller Output
  cipo_in                   1  input        Controller Input
  cipo_out_en               1  output       Tristate control Signal OE=1
  cipo_out                  1  output       Peripheral Output
  sclk_in                   1  input        Serial Clock (Used in Peripheral mode)
  sclk_out_en               1  output       Tristate control Signal OE=1
  sclk_out                  1  output       Serial Clock (Used in Controller mode)
  ncs_in                    1  input        Chip Select ( Active Low ) (Used in Peripheral mode)
  ncs_out_en                1  output       Tristate control Signal OE=1
  ncs_out                   1  output       Chip Select ( Active Low ) (Used in Controller mode)
  ===================  ======  ===========  ====================================================

.. note:: some of these signals may be muxed with other functional IO from different
  ips and users should refer to any pinmux module available on chip





.. tabularcolumns:: |l|l|l|l|

.. _SPI_sb_signals:

.. table:: SPI generated side-band signals generated

  ===================  ======  ===========  ================================================================================================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ================================================================================================================================
  interrupt                 1  output       Signal indicating an interrupt has been raised by the SPI if enabled in control register (SPI_CR2). Signal is connected to PLIC.
  ===================  ======  ===========  ================================================================================================================================


Usage of SPI Module
===================

#. If CRC is being used, Enable CRC Module ``SPI_CR1[13]`` and Specify CRC Length ``SPI_CR1[12]``
#. Specify Polynomial in ``SPI_CRCPR``, Initial & XOR value in ``SPI_CRCINOUT``
#. Specify CRC Reflect options in ``SPI_CR2[17:16]`` and Select Slave ID ``SPI_CR2[26:24]`` 'appropriately'.
#. Specify device Operation ``SPI_CR1[2]`` Mode ``SPI_CR1[1:0]``, Communication Mode ``SPI_CR1[6]``, ``SPI_CR1[14]`` & Format ``SPI_CR1[7]``.
#. Specify Prescalar in ``SPI_PSCR`` and Delays (if needed and applicable in `controller` mode only) in ``SPI_DR``
#. Specify Interrupts (if needed) in ``SPI_CR2[7:5]``
#. Clear the internal States ``SPI_CR1[10]`` and Specify TX and RX bits in ``SPI_CR1[23:16`` , ``SPI_CR1[31:24]`` respectively.
#. Enable SPI in ``SPI_EN``
#. Using Status Register ``SPI_SR`` fill ``TXR`` and / or read ``RXR`` appropriately.
#. Once Transaction is complete ``RX_CRC`` and ``TX_CRC`` can be read (if being used)

Last 4 steps can be iteratively performed per transaction.
