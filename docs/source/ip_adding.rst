######################################
Adding An Xilinx IP into Chromitem SoC
######################################

This tutorial will tell how to integrate a Xilinx  IP  to the chromitem SoC. And will follow the example of adding Ethernet-Lite IP to ChromiteM_SoC.

The entire process can be divided into 4 major steps 
1. Adding required TCL script to generate the IP and modifying existing TCL script as deemed necessary 
2. Modifying  BSV files to include the IP and connecting it to the relevant Bus interface
3. Modifying verilog  files and updating the fpga constraints files 
4. Updating the documentation to account for the new IP 

Prerequisite steps
==================

Clone the repository 

.. code-block:: shell-session

    $ git clone https://gitlab.com/incoresemi/fpga_ports/chromitem_soc.git



Creating TCL for Xilinx based IP
================================

1. All steps in this section will occur in files in the ``chromitem_soc/common_tcl`` directory

This directory contains all the tcl files that are used to generate IP from Xilinx Vivado software.

2. Create a new TCL file ``create_<ip_name>.tcl`` file and `write a script to generate IP <https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug939-vivado-designing-with-ip-tutorial.pdf>`_ that can be used to initialize all the IP related artifacts . 

.. note:: That the fpga part and board settings are set in a top level project file which will in-turn call this new IP TCL file. Thus, fpga part and board settings need not be explicitly taken care by the IP TCL file. However, one is free to check on the following TCL variables to build relevant ip configurations:

      - $board - the board name that Vivado can identify
      - $fpga_part - the xilinx fpga part that is being targeted in the current project.

3. Open create_ip_project.tcl file present in the same directory. Search for the following comment 
: ``# source IP tcls`` and append the following line to source the IP TCL file.


.. code-block:: shell-session

    source $home_dir/common_tcl/create_<ip_name>.tcl

The above will cause the new IP TCL script to be called as part of the execution of the project TCL file. This project TCL file is called while running the `ip_build` target of the top-level Makefile. To check if your tcl script works you can run the following command:

.. code-block:: shell-session

    make ip_build

..
    _There should not be any errors during this process.!

4. If the IP requires a separate clock which is not available from the current clock divider  then open  create_clock_div.tcl which is present in the same directory and create a new clock output from the clock divider and specify the required frequency that your new IP needs.


Editing the BSV files
=====================
To integrate the IP into the SoC, we will need to edit the following list of files

- Soc_map.bsv - to update the memory map and create new slaves
- Soc.bsv - to create slave interfaces, receive and connect interrupts if any to the PLIC
- Soc_instances.bsv - to update the read/write memory map functions and increase PLIC interrupt sources if required by the new IP
- DebugSoc.bsv - to update the slave and interrupt interfaces

All of the above files are located at the root directory of the repository,

1. Start by editing ``Soc_map.bsv``. 

- Goto the section where all the memory map in defined, and now insert your IP’s start and end address. This will look something like this

.. code-block:: shell-session

  `define PLICEnd         'h0FFF_FFFF
  `define OCMBase         'h1000_0000
  `define OCMEnd          'h1000_7FFF
  `define NewIPBase       'hXXXX_XXXX
  `define NewIPEnd        'hXXXX_XXXX

Where NewIPBase indicates the starting address of the IP and NewIPEnd represents the end address of the IP. The Address range should be inclusive of the base and end addresses.

- Goto the section that contains ``AXI4l slave numbers`` and increment the ```define AXI4l_Slaves`` field by one to account for the new IP that is added also while you in this section assign a slave number to the new IP.

.. code-block:: shell-session 

  `define IP_name_slave_num           3

- This step is applicable only if the IP has an interrupt, goto the section that contain ``Peripheral specific parameters`` and increment the ``plic_interrupt_src``  with the number of interrupt source you IP has. 

.. note:: Keep the plic_interrupt_src as a multiple of 4 this is PLIC rule that must be follow, so increment the plic_interrupt_src with some extra value if deemed necessary 

    
- Also define a alias for the size of interrupt of the new IP as follow

.. code-block:: shell-session 

  `define IP_name_interrupt_size     <size_of_interrupt>

2.  Now edit ``Soc_instance.bsv``

- Goto the ``fn_axi4l_rd_map`` function.This function goes into the mkaxi4l_fabric_2 module and helps in mapping of axi4l fabric

An example showing how correctly add replace the NewIPBase and NewIPEnd with your IP primitive 

.. code-block:: shell-session 

  else if (paddr >= `DDRBase       && paddr <= `DDREnd)       return `DDR_slave_num;
  else if (paddr >= `OpenBase      && paddr <= `OpenEnd)      return `Open_slave_num;


  else if (paddr >= `NewIPBase   && paddr <= `NewIPEnd)    return `IP_name_slave_num;
  else return `AXI4lErr_slave_num;
  endfunction:fn_axi4l_rd_map



- Similar thing is to be done for ``fn_axi4l_wr_map`` 

    Completing these steps ensure correct axi4l fabric creation which accounts for your new IP.

3. Now editing Soc.bsv

  Make axi4l interface for your IP + connect it to axi4l crossbar First step is to make an interface for your new IP in your AXI master that will connect with the IP(slave), therefore go to the interface declaration for Soc Ifc_Soc and add an interface of axi4l master type. You can add prefix to your interface that will help you in identifying the related port once the bsv generates a verilog code.

  Below is an example for an axi4l master type interface

.. code-block:: shell-session

    (*prefix="IP_name"*)
    interface Ifc_axi4l_master #(`paddr, 32, `User) <IP_name_slave>;

Now you have to initialize this interface as a part of your axi4l crossbar, you can do this by the following line of code inside the mkSoc module

.. code-block:: shell-session

    interface <IP_name_slave>   = axi4l_xbar.v_to_slaves[`IP_name_slave_num];


- If your IP has interrupt you need to accept that signal by creating an Action method, and connecting that interrupt to the PLIC (which handles all the Soc interrupts).

 Example of this method is shown below:

.. code-block:: shell-session

 (*always_enabled,always_ready, prefix="<prefix>"*)
  method Action <method_name>Bit#(1) int_in);

- The tag ``(*always_enabled,always_ready, prefix="<prefix>"*)`` removes the enable and ready signal from the method and add a prefix that makes it easier for the user to identify the group the signal belongs to.

- After creating the method you need to declare a wire for interrupt signal inside the ``mkSoc`` module 

.. code-block:: shell-session

    Wire#(Bit#(<intrrupt_size>)) <name_of_the_intrruppt_signal> <- mkBypassWire()


- Drive this wire to hold the interrupt signals using the method definition below: 

.. code-block:: shell-session
  
  method Action ma_eth_inp(Bit#(1) int_in);
        <name_of_the_intrruppt_signal> <= int_in;
  endmethod:ma_eth_inp

    
- Now we have created the interrupt wire all that remains is to connect it with the PLIC.

- Goto inside the mkSoc module and define a variable for the interrupt signal you need to follow the convention, you will see block of code that creates variable for interrupt for various peripheral similarly create a variable as follow

.. code-block:: shell-session

 let next_periph_int =  <name-of-last-interrupt-variable>> + <size>;


- Where <size> is the size of interrupt of peripheral defined above in the code.

- Now we need connect the the interrupt to PLIC, which are done by passing the signal through mk_gateway module, which can be done as follow

.. code-block:: shell-session 

   for(Integer idx = 0; idx < `<IP_interrupt_size> ; idx = idx + 1) begin
        let <ip_name>_idx = idx + next_periph_int;
        int_gwy[eth_idx] <- mk_gateway(<name_of_the_intrruppt_signal>, clocked_by clk, reset_by rst,
                                    '0, sb_int_complete[<ip_name>_idx] );
    end 


For the argument details of the gateway module refer the gateway code which is present inside the location `/<repo_dir>/devices/plic/gateway `

4. Finally editing DebugSoc.bsv

- Similar thing is done for the DebugSoc.bsv as Soc.bsv

- Create a master interface for the axi4l and method for handling interrupt of your IP inside the interface ``Ifc_DebugSoc``

  The interface definition of the method will change here as DebugSoc.bsv sits on top of Soc.bsv and acts as a interface between the FPGA and soc signal, the following code block shows an example definition for the method defined inside the mkDebugSoc module 

.. code-block:: shell-session 

  method Action <method_name>(Bit#(<interrupt_size>)intc_irpt);
    soc.<soc_method_name>(intc_irpt);
  endmethod  


Similarly the master interface will be connected in the following way

.. code-block:: shell-session

  interface <slave_interface_name_inside_DebugSoc>  = soc.slave_interface_name_inside_soc> ;




- After editing all these you need to run the following command

.. code-block:: shell-session

  make generate_verilog


If you did everything correctly no error should pop up after the execution of this command.

.. note:: To ensure what you did was taken into consideration go to verilog file generated(top_dir/chromitem_soc/build/hw/verilog) and open mkSoc.v file you should see axi4l signals for your ip in the port list of the mkSoc module.

Updating Fpga files (fpga_top.v and constraints.xdc)
====================================================
- Fpga related files can be found in the location 

.. code-block:: shell-session

 cd top_dir/chromitem_soc/fpga/board_name


Where top_dir represents the directory where you cloned the repository, and board_name represents the fpga board you are working with.

- A brief description of these files can be found below

  - Fpga_top.v : this is top verilog file that deals with programming the fpga, it instantiate all the parts of for the fpga and connects them together

  - Constraints.xdc : this files deals with the physical pin mapping for the fpga.

- First we edit the constraints.xdc file which deals with the pin mapping for FPGA, therefore if your IP is using any FPGA pins then specify those pin here

- Open the constraints file and add your pins constraints. You can also find a general constraints file from your FPGA vendor that has all the pin predeclared for you and you just have to uncomment the required lines for the desired pin.

Changing fpga_top can be divided into 4 parts


 1. **Adding physical pins that IP is using as an input output ports of fpga_top** module Declare all the pin that the IP uses as input and output port in the fpga_top module, as these ports acts as an interface between the fpga pin and the board.


 2. **Adding interrupt wire** If the IP has interrupt the declare them as internal wire to the fpga_top module.

 3. **Adding axi4l related pins.** Also inside fpga_top module declare all the pin that are required for axi4l communication as wires

 4. **Connecting the pins** Since now we have declared all the pin for IP we need to properly instantiate the IP and connect these pin accordingly

    - Connecting the clock output of clock divider to IP clock wire If your ip needs a clock then the output for clock comes from the clock divider therefore connect the output of clock to the IP clock pin in the clock divider instantiation.

    - Connecting the axi4l pin with the port of DebugSoc module As described earlier DebugSoc is the module that acts as an interface between the FPGA and SoC therefore all the IP signals(axi4l + interrupt) will have to be connected here in the module instantiation of DebugSoc

    - Connecting interrupt 

    - Initializing the IP + taking the declaration method from .veo file generated by the IP Now all that remains in IP instantiation, for that use the template provided by the IP files ``(top_dir/chromitem_soc/build/hw/fpga/manage_ip/manage_ip.runs/<ip_name>)`` where you will find .dcp zip file that contains a .v (verilog file) providing you with a a empty module with port declaration file which causes synthesis tools to infer a black box for IP, refer that file and instantiate the IP and connect it with the pin that you declared above.

That make the end of the editing fpga related documents now you can build the code and test it out on an FPGA . follow the steps mentioned `here <https://chromitem-soc.readthedocs.io/en/stable/getting_started.html#building-the-fpga-target>`_ from step 5 onwards.

Updating the documentation for the change
=========================================

1. Update the block diagram by opening the `drawio <https://www.diagrams.net/>`_ file present in ``top_dir/chromitem_soc/docs/image_srcs`` in the `web <https://app.diagrams.net/>`_ browser link

2. Update docs/source/memory_map.rst with appropriate memory address To document your changes in memory map, specify the memory space the new IP is using in the file above

3. Update fpga/arty100t/arty-100t.rst with the new pin information To document your changes in pin mapping, of fpga specify the pins used by the new IP is using in the file above

4. Also update the PLIC section for any new interrupts that you might have connected to it, in the plic_irq_mapping.rst file present in /chromitem_soc/docs/source location, with the interrupt ID, peripheral that is using that interrupt id and a brief description of the interrupt


For IP which you have a source code (RTL file)
==============================================
There might me the case where you have the IP source file in RTL file, for thoses cases you need to write a bluespec wrapper which will encapsulate the RTL IP and make it seems as it is a bluespec file.

- Write a bluespec `wrapper for the RTL file of the IP <http://csg.csail.mit.edu/6.S078/6_S078_2012_www/resources/reference-guide.pdf#page=126&zoom=100,120,609>`_
