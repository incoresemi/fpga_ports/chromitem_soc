.. _memory_map:

##########
Memory Map
##########

The memory map of the SoC is shown in :numref:`memory_map_table`

.. tabularcolumns:: |l|l|l|l|

.. _memory_map_table:

.. table:: Memory Map of the Chormite M SoC

  ============ =========== ========== ============================
  Base Address Top Address Attributes Description
  ============ =========== ========== ============================
  0x0000_0100  0x0000_010F R-X        Debug Loop
  0x0000_0200  0x0000_0203 R          Boot config
  0x0001_0000  0x0001_0FFF R-X-C      Boot Rom
  0x0001_1300  0x0001_13FF R-W        Uart-0
  0x0002_0000  0x2000_00FF R-W        GPIOs
  0x0002_0100  0x0002_01FF R-W        SPI0 (Used for Flash)
  0x0002_0200  0x0002_02FF R-W        SPI1
  0x0002_0300  0x0002_03FF R-W        SPI2
  0x0003_0000  0x0003_00FF R-W        PWM 6 channels
  0x0004_0000  0x0004_00FF R-W        QSPI Config Space
  0x0200_0000  0x020B_FFFF R-W        Core Local Interrupt Controller
  0x0C00_0000  0x0FFF_FFFF R-W        Platform Level Interrupt Controller
  0x1000_0000  0x1000_7FFF R-W-X-C    On Chip Memory of 16KB
  0x1000_8000  0x1000_BFFF R-W        Ethernet Lite
  0x8000_0000  0x8FFF_FFFF R-W-X-C    DDR of 256MB
  0x9000_0000  0x9FFF_FFFF R-X        QSPI memory space
  0xA000_0000  0xFFFF_FFFF R-W-X      Open AXI-4 Slave
  ============ =========== ========== ============================

- R : Read Access
- W : Write Access
- X : Execute Access
- C : Cacheable Access
