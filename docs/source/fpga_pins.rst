.. _fpga_pins:

###############
Supported FPGAs
###############

The pin mapping of the SoC signals to the board pins is described
for each supported board:

.. include:: arty-100t.rst
